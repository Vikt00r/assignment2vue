/*
Defines the routes in the project and which paths that are used.
*/

import VueRouter from "vue-router";
import StartScreen from "./components/StartScreen/StartScreen";
import QuizPage from "./components/QuizPage/QuizPage";
import ResultPage from "./components/ResultPage/ResultPage";
const routes = [
  {
    path: "/",
    name: "StartScreen",
    component: StartScreen
  },

  {
    path: "/quiz",
    name: "QuizPage",
    component: QuizPage,
  },

  {
    path: "/result",
    name: "ResultPage",
    component: ResultPage
  }
];


const router = new VueRouter({ routes });

export default router;