/*
This class contains of all the fetch methods for the project.
Two const which contains of two different urls used when fetching.
*/

const getAllCategories = "https://opentdb.com/api_category.php"
const getQuestions = "https://opentdb.com/api.php?amount=";

/*
getAllCategory: Fetching all the different categories that exists in the given api.
returns: all the categories that was return from the fetch.
*/
export async function getAllCategory() {
    try {
        let response = await fetch(getAllCategories);
        let data = await response.json();
        return data.trivia_categories;
    } catch (error) {
        console.log(error)
    }
}

/*
fetchQuestions: Fetching questions, questions varies depending on the input.
building the string depending on what the parameters consists of. 
return: questions based on the input parameters.
*/
export async function fetchQuestions(numberOfQuestions, categoryId, difficultyType) {
    difficultyType = difficultyType.toLowerCase()
    let category = `&category=${categoryId}`;
    let difficulty = `&difficulty=${difficultyType}`;

    if (categoryId === 8) {
        category = "";
    }
    if (difficultyType === "any") {
        difficulty = "";
    }
    try {
        let response = await fetch(getQuestions + numberOfQuestions + category + difficulty);
        let data = await response.json()
        console.log(data.results);
        return data.results;

    } catch (error) {
        console.log(error);
    }



}