/*
Data consisting of the different difficulties that is available. 
*/
const difficulties = [
    {
        "type": "Any",
    },
    {
        "type": "Easy"
    },
    {
        "type": "Medium"
    },
    {
        "type": "Hard"
    }
];

export default difficulties;